# Set master image
FROM php:fpm-alpine

# Set working directory
WORKDIR /var/www/html

# Install PHP Composer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

# Copy existing application directory
COPY . .

EXPOSE 8000
ENTRYPOINT [ "php", "artisan", "serve" ]
CMD ["--host", "0.0.0.0"]
